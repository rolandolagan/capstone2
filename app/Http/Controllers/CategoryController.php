<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index(){
        $categories = Category::all();
        return view('manage-categories', compact('categories'));
    }
    public function create(){
        return view('add-category');
    }
    public function store(Request $request){
        $rules = array("name"=>"required");
        $this->validate($request, $rules);
        $category = new Category;
        $category->name = $request->name;
        $category->save();
        return redirect('/manage-categories');
    }
    public function edit($id){
        $category = Category::find($id);
        return view('update-category',compact('category'));
    }
    public function update($id, Request $request){
        $category = Category::find($id);
        $category->name = $request->name;
        $category->save();
        return redirect('/manage-categories');
    }
    public function destroy($id){
        $category = Category::find($id);
        $category->delete();
        return back();
    }
}
