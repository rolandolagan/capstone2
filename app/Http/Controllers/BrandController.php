<?php

namespace App\Http\Controllers;

use App\Brand;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    public function index(){
        $brands = Brand::all();
        return view('manage-brands',compact('brands'));
    }
    public function create(){
        return view('add-brand');
    }
    public function store(Request $request){
        $rules = array("name"=>"required");
        $this->validate($request, $rules);
        $brand = new Brand;
        $brand->name = $request->name;
        $brand->save();
        return redirect('/manage-brands');
    }
    public function edit($id){
        $brand = Brand::find($id);
        return view('/update-brand',compact('brand'));
    }
    public function update($id, Request $request){
        $brand = Brand::find($id);
        $brand->name = $request->name;
        $brand->save();
        return redirect('/manage-brands');
    }
    public function destroy($id){
        $brand = Brand::find($id);
        $brand->delete();
        return back();
    }
}
