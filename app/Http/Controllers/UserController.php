<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use App\User;
use App\Role;
use App\Status;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index(){
        $users = User::all();
        $roles = Role::all();
        $statuses = Status::all();
        return view('manage-users', compact('users','roles','statuses'));
    }
    public function create(){
        $roles = Role::all();
        $statuses = Status::all();
        return view('add-user',compact('roles','statuses'));
    }
    public function store(Request $request){
        $this->validation($request);
        $user = new User;
        // or $request['password'] = bcrypt($request->password);
        // User::create($request->all());
        $user->fname = $request->fname;
        $user->lname = $request->lname;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->role_id = $request->role_id;
        $user->status_id = $request->status_id;
        $user->save();
        return redirect('/manage-users');
    }
    public function edit($id){
        $roles = Role::all();
        $statuses = Status::all();
        $user = User::find($id);
        return view('update-user', compact('user','roles','statuses'));
    }
    public function update($id, Request $request){
        $user = User::find($id);
        if($user->email === $request->email){
            if(empty($request->password)){
                $rules = array(
                    'fname' => 'required|max:255',
                    'lname' => 'required|max:255',
                    'email' => 'required|email|max:255',
                );
                $this->validate($request, $rules);
                $user->fname = $request->fname;
                $user->lname = $request->lname;
                $user->email = $request->email;
                $user->role_id = $request->role_id;
                $user->status_id = $request->status_id;
            }elseif(!empty($request->password)){
                if(Hash::check($request['password'], $user->password)){
                    $rules = array('password' => 'required|string|min:8|confirmed|max:255',);
                    $this->validate($request, $rules);
                    $user->password = Hash::make($request->password);
                    Session::flash("message","New password must be different from Old password");
                    return back();
                }
                else{
                    $rules = array(
                        'fname' => 'required|max:255',
                        'lname' => 'required|max:255',
                        'email' => 'required|email|max:255',
                        'password' => 'string|min:8|confirmed|max:255',
                    );
                        $this->validate($request, $rules);
                        $user->fname = $request->fname;
                        $user->lname = $request->lname;
                        $user->email = $request->email;
                        $user->password = Hash::make($request->password);
                        $user->role_id = $request->role_id;
                        $user->status_id = $request->status_id;
                }
            }
        }else{
            if(empty($request->password)){
                $rules = array(
                    'fname' => 'required|max:255',
                    'lname' => 'required|max:255',
                    'email' => 'required|email|unique:users|max:255',
                );
                $this->validate($request, $rules);
                $user->fname = $request->fname;
                $user->lname = $request->lname;
                $user->email = $request->email;
                $user->role_id = $request->role_id;
                $user->status_id = $request->status_id;
            }elseif(!empty($request->password)){
                if(Hash::check($request['password'], $user->password)){
                    $rules = array('password' => 'required|string|min:8|confirmed|max:255',);
                    $this->validate($request, $rules);
                    $user->password = Hash::make($request->password);
                    Session::flash("message","New password must be different from Old password");
                    return back();
                }
                else{
                    $rules = array(
                        'fname' => 'required|max:255',
                        'lname' => 'required|max:255',
                        'email' => 'required|email|unique:users|max:255',
                        'password' => 'string|min:8|confirmed|max:255',
                    );
                        $this->validate($request, $rules);
                        $user->fname = $request->fname;
                        $user->lname = $request->lname;
                        $user->email = $request->email;
                        $user->password = Hash::make($request->password);
                        $user->role_id = $request->role_id;
                        $user->status_id = $request->status_id;
                }
            }
        }
        
        $user->save();
        return redirect('/manage-users');
    }
    public function validation($request){

        return $this->validate($request,[
            'fname' => 'required|max:255',
            'lname' => 'required|max:255',
            'email' => 'required|email|max:255',
            'password' => 'required|string|min:8|confirmed|max:255',
        ]);
    }
    public function destroy(Request $request){
        $id = $request->user_id;
        $user = User::find($id);
        $user->delete();
        return back();
    }

    public function profile(){
        return view('manage-profile');
    }

    public function editProfile($id){
        $user = User::find($id);
        return view('update-profile', compact('user'));
    }
    public function updateProfile($id, Request $request){
        $user = User::find($id);
        if($user->email === $request->email){
            $rules = array(
                'fname' => 'required|max:255',
                'lname' => 'required|max:255',
                'email' => 'required|email|max:255',
            );
            $this->validate($request, $rules);
            $user->fname = $request->fname;
            $user->lname = $request->lname;
            $user->email = $request->email;
        }else{
            $rules = array(
                'fname' => 'required|max:255',
                'lname' => 'required|max:255',
                'email' => 'required|email|unique:users|max:255',
            );
            $this->validate($request, $rules);
            $user->fname = $request->fname;
            $user->lname = $request->lname;
            $user->email = $request->email;
        }
        
        $user->save();
        return redirect('/manage-profile');
    }

    public function editPass($id){
        return view('update-password');
    }
    public function updatePass($id, Request $request){
        $user = User::find($id); 
        if(Hash::check($request['password'], Auth::user()->password)){
            $rules = array('password' => 'required|string|min:8|confirmed|max:255',);
            $this->validate($request, $rules);
            $user->password = Hash::make($request->password);
            Session::flash("message","New password must be different from Old password");
            return back();
        }else{
            $rules = array('password' => 'required|string|min:8|confirmed|max:255',);
            $this->validate($request, $rules);
            $user->password = Hash::make($request->password);
            $user->save();
            return redirect('/manage-profile');
        }
        // if(Hash::check($user->password)){
        //     $rules = array('password' => 'required|string|min:8|confirmed|max:255',);
        //     $this->validate($request, $rules);
        //     $user->password = Hash::make($request->password);
    }
}
