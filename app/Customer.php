<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    public function payment(){
        return $this->belongsTo('App\Payment');
    }
    public function product(){
        return $this->belongsTo('App\Product');
    }
}
