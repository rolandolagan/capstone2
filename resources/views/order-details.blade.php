@extends('layouts.template')

@section('title', 'Orders')

@section('content')
<div class="vcss-main-div">
    <header class="vcss-header">
        <h2 class="h-header">Inventory Management System</h2>
    </header>
    <section class="vcss-section">
        <div class="vcss-div">
            <h3>Order Details</h3>
            <hr>
            <div class="ord-wrapper d-flex">
                <div class="ord-prod-div">
                    <legend>Order information</legend>
                    <div>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Brand</th>
                                    <th>Description</th>
                                    <th>Price</th>
                                    <th>Qty</th>
                                    <th>Sub Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($order->products as $product)
                                <tr>
                                    <td>{{$product->id}}</td>
                                    <td>{{$product->name}}</td>
                                    <td>{{$product->brand->name}}</td>
                                    <td>{{$product->description}}</td>
                                    <td>{{$product->selling_price}}</td>
                                    <td>{{$product->pivot->quantity}}</td>
                                    <td>{{$product->selling_price * $product->pivot->quantity}}</td>
                                </tr>
                                @endforeach
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td><h5>Total:</h5></td>
                                    <td></td>
                                    <td>{{$order->total}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    
                </div>

                <div class="ord-cus-div">
                    <legend>Customer Information</legend>
                    <div>
                        <label for="">Fullname</label>
                        <input class="form-control" type="text" value="{{$order->customer->name}}" readonly>
                    </div>
                    <div>
                        <label for="">Address</label>
                        <input class="form-control" type="text" value="{{$order->customer->address}}" readonly>
                    </div>
                    <div>
                        <label for="">Email</label>
                        <input class="form-control" type="text" value="{{$order->customer->email}}" readonly>
                    </div>
                    <div>
                        <label for="">Contact No.</label>
                        <input class="form-control" type="text" value="{{$order->customer->contact}}" readonly>
                    </div>
                    <div>
                        <label for="">Payment Status</label>
                        <input class="form-control" value="{{$order->payment->name}}" readonly>
                    </div>
                    
                    <div class="form-group d-flex justify-content-center align-items-center">
                        <form class="mx-2" action="/invoice/{{$order->id}}" method="GET">
                            <input class="ord-pay" type="hidden" name="payment_id">
                            <button type="submit" class="btn btn-danger ">PDF</button>
                        </form>
                        <a class="btn btn-back vcss-btn mx-2" href="/manage-orders" type="button">Back</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <footer class="vcss-footer">
        <p class="f-text">Created By: Me</p>
        <p class="f-text">Powered by Laravel 7v</p>
    </footer>
</div>
{{-- <script type="text/javascript">
    const selectPay = document.getElementById('ord-select-pay')
    selectPay.addEventListener('click', function(){
        document.querySelector('.ord-pay').value = $('option:selected', this).attr('value')
    })
</script> --}}
@endsection