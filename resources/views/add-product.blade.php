@extends('layouts.template')

@section('title', 'Products')

@section('content')
<div class="vcss-main-div">
    <header class="vcss-header">
        <h2 class="h-header">Inventory Management System</h2>
    </header>
    <section class="vcss-section">
        <div class="vcss-div">
            <h3>Add Product</h3>
            <hr>
            <div class="product-edit-wrapper">
                <legend>Product Information</legend>
                <hr>
                <form class="form-scroll" action="/add-product" method="POST">
                    @csrf
                    <fieldset>
                        <div class="form-group">
                            <label for="">Product Name</label>
                            <input class="form-control" type="text" name="name" required>
                        </div>
                        <div class="form-group">
                            <label for="">Category</label>
                            <select class="form-control" name="category_id">
                                @foreach ($categories as $category)
                                <option value="{{$category->id}}">{{$category->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Brand</label>
                            <select class="form-control" name="brand_id">
                                @foreach ($brands as $brand)
                                <option value="{{$brand->id}}">{{$brand->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Description</label>
                            <input class="form-control" type="text" name="description" required>
                        </div>
                        <div class="form-group">
                            <label for="">Quantity</label>
                            <input class="form-control" type="number" name="quantity" required>
                        </div>
                        <div class="form-group">
                            <label for="">Buying Price</label>
                            <input class="form-control" type="number" name="buying_price" required>
                        </div>
                        <div class="form-group">
                            <label for="">Selling price</label>
                            <input class="form-control" type="number" name="selling_price" required>
                        </div>
                        <div class="form-group">
                            <label for="">Supplier</label>
                            <select class="form-control" name="supplier_id">
                                @foreach ($suppliers as $supplier)
                                <option value="{{$supplier->id}}">{{$supplier->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block">Submit</button>
                            <a class="btn btn-back vcss-btn btn-block" href="/manage-products" type="button">Back</a>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </section>
    <footer class="vcss-footer">
        <p class="f-text">Created By: Me</p>
        <p class="f-text">Powered by Laravel 7v</p>
    </footer>
</div>
@endsection