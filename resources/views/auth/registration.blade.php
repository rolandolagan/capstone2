@extends('layouts.LR-template')

@section('title','Registration')

@section('content')
<div class="vcss-container container">
    <div class="row">
        <div class="vcss-form col-lg-6 offset-lg-3">
            <form action="/register" method="POST">
                @csrf
                <fieldset>
                <legend>Register</legend>
                    <div class="form-group">
                        <label for="">First Name</label>
                        <input class="form-control" type="text" name="fname" value="{{old('fname')}}" required>
                    </div>
                    <div class="form-group">
                        <label for="">Last Name</label>
                        <input class="form-control" type="text" name="lname" value="{{old('lname')}}" required>
                    </div>
                    <div class="form-group">
                        <label for="">Email</label>
                        <input id="email" class="form-control @error('email') is-invalid @enderror" type="text" name="email" value="{{old('email')}}" required>
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{$message}}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="">Password</label>
                        <input id="password" class="form-control @error('password') is-invalid @enderror" type="password" name="password" required>
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{$message}}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="">Confirm</label>
                        <input class="form-control" type="password" name="password_confirmation" required>
                    </div>
                    <div class="form-group">
                        <select class="form-control" name="role_id">
                            @foreach ($roles as $role)
                            @if ($role->id != 1)
                            <option value="{{$role->id}}">{{$role->name}}</option>
                            @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <select class="form-control" name="status_id">
                            @foreach ($statuses as $status)
                            <option value="{{$status->id}}">{{$status->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary btn-block" type="submit">Register</button>
                    </div>
                    <p class="text-center">Already Registered? <a href="/login"><span>Login</span></a></p>
                </fieldset>
            </form>
        </div>
    </div>
</div>
@endsection