@extends('layouts.LR-template')

@section('title','Login')

@section('content')
<div class="vcss-container container">
    <h2 class="login-title">Inventory Management System</h2>
    <div class="vcss-form">
        <form action="/login" method="POST">
            @csrf
            <fieldset>
            <legend>Login</legend>
                @if(count($errors))
                    @foreach($errors->all() as $error)
                        <p class="text-danger">{{$error}}</p>
                    @endforeach
                @endif
                <div class="form-group">
                    <label for="">Email</label>
                    <input id="email" class="form-control @error('email') is-invalid @enderror" type="text" name="email" value="{{old('email')}}">
                    {{-- @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{$message}}</strong>
                        </span>
                    @enderror --}}
                </div>
                <div class="form-group">
                    <label for="">Password</label>
                    <input id="password" class="form-control @error('password') is-invalid @enderror" type="password" name="password">
                    {{-- @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{$message}}</strong>
                        </span>
                    @enderror --}}
                </div>
                <div class="form-group">
                    <button class="btn btn-primary btn-block" type="submit">Login</button>
                </div>
                {{-- <p class="text-center">Not Registered? <a href="/register"><span>Register</span></a></p> --}}
            </fieldset>
        </form>
    </div>
</div>
@endsection