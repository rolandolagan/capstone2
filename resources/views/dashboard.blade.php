@extends('layouts.template')

@section('title', 'Dashboard')

@section('content')
<div class="vcss-main-div">
    <header class="vcss-header">
        <h2 class="h-header">Inventory Management System</h2>
        <div id="time"></div>
    </header>
    
    <section class="vcss-section">
        <div class="vcss-div">
            <div class="dash">
                <div class="card bg-info">
                    <h5 class="card-title">{{$totalprod}}</h5>
                    <h4>Total Products</h4>
                    <img class="dash-icons" src="{{asset('images/icons/totalprod.svg')}}" alt="">
                </div>
                <div class="card bg-success">
                    <h5 class="card-title">{{$paid}}</h5>
                    <h4>Total Paid Orders</h4>
                    <img class="dash-icons" src="{{asset('images/icons/totalpaid.svg')}}" alt="">
                </div>
                <div class="card bg-success">
                    <h5 class="card-title">{{$sale}}</h5>
                    <h4>Total Sales</h4>
                    <img class="dash-icons" src="{{asset('images/icons/sale.svg')}}" alt="">
                </div>
                <div class="card bg-warning">
                    <h5 class="card-title">{{$users}}</h5>
                    <h4>Total Users</h4>
                    <img class="dash-icons" src="{{asset('images/icons/totaluser.svg')}}" alt="">
                </div>
            </div>
        </div>
    </section>
    
    <footer class="vcss-footer">
        <p class="f-text">Created By: Me</p>
        <p class="f-text">Powered by Laravel 7v</p>
    </footer>
</div>
<script>
    const getTime = document.getElementById('time')
    const get = async() =>{
        const res = await fetch('http://worldtimeapi.org/api/timezone/Asia/Manila')
        const data = await res.json()
        console.log(data)
        getTime.innerHTML = data.datetime
    }
    get()

    const sounds = async() =>{
        const res = await fetch('https://freesound.org/apiv2/search/text/?query=piano&token=')
        constdata = await res.json()
        console.log(res)
        console.log(data)
    }
</script>
@endsection