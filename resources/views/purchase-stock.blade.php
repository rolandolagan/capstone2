@extends('layouts.template')

@section('title', 'Products')

@section('content')
<div class="vcss-main-div">
    <header class="vcss-header">
        <h2 class="h-header">Inventory Management System</h2>
    </header>
    
    <section class="vcss-section">
        <div class="vcss-div">
            <h3>Purchase Stock</h3>
            <hr>
            <div class="product-edit-wrapper">
                <legend>Product Information</legend>
                <hr>
                <form class="form-scroll" action="/purchase-stock" method="POST">
                    @csrf
                    @method('PATCH')
                    <div class="form-group">
                        <label for="">Product Name</label>
                        <input type="hidden" name="name" >
                        <select id="pur-sel" class="form-control" name="product_id">
                            @foreach ($products as $product)
                            <option value="{{$product->id}}" data-cat="{{$product->category_id}}" data-brand="{{$product->brand_id}}" data-des="{{$product->description}}" data-selp="{{$product->selling_price}}" data-buyp="{{$product->buying_price}}" data-sup-name="{{$product->supplier->name}}" data-sup-id="{{$product->supplier_id}}">{{$product->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Category</label>
                        <input class="form-control pur-cat" readonly>
                    </div>
                    <div class="form-group">
                        <label for="">Brand</label>
                        <input class="form-control pur-brand" readonly>
                    </div>
                    <div class="form-group">
                        <label for="">Description</label>
                        <input class="form-control pur-des" readonly>
                    </div>
                    <div class="form-group">
                        <label for="">Quantity</label>
                        <input class="form-control pur-qty" type="number" name="quantity" required>
                    </div>
                    <div class="form-group">
                        <label for="">Buying Price</label>
                        <input class="form-control pur-buyp" type="number" name="buying_price" required>
                    </div>
                    <div class="form-group">
                        <label for="">Selling price</label>
                        <input class="form-control pur-selp" type="number" name="selling_price" required>
                    </div>
                    <div class="form-group">
                        <label for="">Supplier</label>
                        <input class="pur-sup-id" type="hidden" name="supplier_id">
                        <input class="form-control pur-sup-name" type="text" readonly>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-prime vcss-btn btn-block" type="submit">Submit</button>
                        <a class="btn btn-back vcss-btn btn-block" href="/manage-products" type="button">Back</a>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <footer class="vcss-footer">
        <p class="f-text">Created By: Me</p>
        <p class="f-text">Powered by Laravel 7v</p>
    </footer>
    
</div>
<script type="text/javascript" src="{{asset('js/purchase.js')}}" defer></script>
@endsection