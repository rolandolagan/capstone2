@extends('layouts.template')

@section('title', 'Orders')

@section('content')
<div class="vcss-main-div">
    <header class="vcss-header">
        <h2 class="h-header">Inventory Management System</h2>
    </header>
    <section class="vcss-section">
        <div class="vcss-div">
            <h3>Manage Orders</h3>
            <hr>
            <div class="top-bar">
                <form action="/add-order" method="GET">
                    @csrf
                    <button class="btn vcss-btn btn-prime" type="submit">Add Order</button>
                </form>
                <form class="form-inline mx-2" action="/manage-orders" method="GET">
                    @csrf
                    <select class="form-control" name="price">
                        <option value="asc">Asc</option>
                        <option value="desc">Desc</option>
                    </select>
                    <button class="btn  my-2" type="submit">Search</button>
                </form>
            </div>
            <div>
                <table class="text-center table table-striped my-2">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Customer Name</th>
                            <th>Customer Address</th>
                            <th>Customer Contact No.</th>
                            <th>Date of Order</th>
                            <th>Total</th>
                            <th>Status</th>
                            <th>Issued By</th>
                            <th>Details</th>
                            @auth
                            @if (Auth::user()->role_id <= 2)
                            <th>Action</th>
                            @endif
                            @endauth
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($orders as $order)
                        <tr>
                            <td>{{$order->id}}</td>
                            <td>{{$order->customer->name}}</td>
                            <td>{{$order->customer->address}}</td>
                            <td>{{$order->customer->contact}}</td>
                            <td>{{$order->created_at}}</td>
                            <td>{{$order->total}}</td>
                            <td>{{$order->payment->name}}</td>
                            <td>{{$order->user->fname}}</td>
                            <td>
                                <form action="/order-details/{{$order->id}}" method="GET">
                                    <button class="btn vcss-btn">
                                        <img class="action-icons" src="{{asset('images/icons/view.svg')}}" alt="">
                                    </button>
                                </form>
                            </td>
                            @auth
                            @if (Auth::user()->role_id <= 2)
                            <td class="td-action">
                                @if ($order->payment_id != 2)
                                <form action="/update-order/{{$order->id}}" method="GET">
                                    @csrf
                                    <button class="btn vcss-btn">
                                        <img class="action-icons" src="{{asset('images/icons/edit.svg')}}" alt="">
                                    </button>
                                </form>
                                <form action="/delete-order/{{$order->id}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn vcss-btn">
                                        <img class="action-icons" src="{{asset('images/icons/delete.svg')}}" alt="">
                                    </button>
                                </form>
                                @endif
                            </td>
                            @endif
                            @endauth
                        </tr>   
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <footer class="vcss-footer">
        <p class="f-text">Created By: Me</p>
        <p class="f-text">Powered by Laravel 7v</p>
    </footer>
</div>
@endsection