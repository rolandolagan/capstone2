<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- bootstrap css -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <!-- custom css -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous" defer></script>
    <script src="{{asset('js/bootstrap.min.js')}}" defer></script>
    <title>@yield('title')</title>
</head>
<body>
<style>
    body{
        background-image: url({{asset('images/background/main.jpg')}});
        background-repeat: no-repeat;
        background-size: cover;
    }
</style>
@yield('content')

</body>
</html>