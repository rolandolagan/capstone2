@extends('layouts.template')

@section('title', 'Products')

@section('content')
<div class="vcss-main-div">
    <header class="vcss-header">
        <h2 class="h-header">Inventory Management System</h2>
    </header>
    
    <section class="vcss-section">
        <div class="vcss-div">
            <h3>Manage Products</h3>
            <hr>
            <div class="top-bar">
                <div class="d-flex">
                    @auth
                    @if (Auth::user()->role_id <= 2)
                    <form class="" action="/add-product" method="GET">
                        @csrf
                        <button class="btn vcss-btn btn-prime" type="submit">Add Product</button>
                    </form>
                    @endif
                    @endauth
                    <form class="mx-2" action="/purchase-stock" method="GET">
                        @csrf
                        <button class="btn vcss-btn btn-prime" type="submit">Purchase Stock</button>
                    </form>
                </div>
                <div class="d-flex">
                    {{-- <div class="form-inline mx-2">
                        <select id="select-cat" class="form-control mx-2" name="category_id">
                            <option value="">Category</option>
                            @foreach ($categories as $category)
                            <option value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach
                        </select>
                        <button class="btn  my-2" type="submit">Search</button>
                    </div> --}}
                    {{-- <form class="form-inline mx-2" action="/manage-products" method="GET">
                        @csrf
                        
                        <select class="form-control" name="sort">
                            <option value="asc">Asc</option>
                            <option value="desc">Desc</option>
                        </select>
                        <button class="btn my-2" type="submit">Sort by</button>
                    </form> --}}
                    <form class="form-inline mx-2" action="/manage-products" method="GET">
                        @csrf
                        <select class="form-control mx-2" name="category_id">
                            <option value="">Category</option>
                            @foreach ($categories as $category)
                            <option value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach
                        </select>
                        <select class="form-control mx-2" name="brand_id">
                            <option value="">Brand</option>
                            @foreach ($brands as $brand)
                            <option value="{{$brand->id}}">{{$brand->name}}</option>
                            @endforeach
                        </select>
                        <button class="btn  my-2" type="submit">Search</button>
                    </form>
                </div>
            </div>
            <div>
                <table class="text-center table table-striped my-2">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Category</th>
                            <th>Brand</th>
                            <th>Description</th>
                            <th>Supplier</th>
                            <th>Qty</th>
                            <th>Buying Price</th>
                            <th>Selling Price</th>
                            @auth
                            @if (Auth::user()->role_id <= 2)
                            <th>Action</th>
                            @endif
                            @endauth
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($products as $product)
                        <tr>
                            <td>{{$product->id}}</td>
                            <td>{{$product->name}}</td>
                            <td>{{$product->category->name}}</td>
                            <td>{{$product->brand->name}}</td>
                            <td>{{$product->description}}</td>
                            <td>{{$product->supplier->name}}</td>
                            <td>{{$product->quantity}}</td>
                            <td>{{$product->buying_price}}</td>
                            <td>{{$product->selling_price}}</td>
                            @auth
                            @if (Auth::user()->role_id <= 2)
                            <td class="td-action">
                                <form action="/update-product/{{$product->id}}" method="GET">
                                    @csrf
                                    <button class="btn vcss-btn">
                                        <img class="action-icons" src="{{asset('images/icons/edit.svg')}}" alt="">
                                    </button>
                                </form>
                                <form action="/delete-product/{{$product->id}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn vcss-btn">
                                        <img class="action-icons" src="{{asset('images/icons/delete.svg')}}" alt="">
                                    </button>
                                </form>
                            </td>
                            @endif
                            @endauth
                        </tr>   
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <footer class="vcss-footer">
        <p class="f-text">Created By: Me</p>
        <p class="f-text">Powered by Laravel 7v</p>
    </footer>
</div>
{{-- <script>
    const category = document.getElementById('select-cat')

    category.addEventListener('change',function(e){
        const categoryId = e.target.value
        fetch("/manage-products/category_id=" + categoryId)
        .then(function(res){
            return res.text()
        }).then(function(data){
            console.log(data)
        })
    })
</script> --}}
@endsection