@extends('layouts.template')

@section('title', 'Customers')

@section('content')
<div class="vcss-main-div">
    <header class="vcss-header">
        <h2 class="h-header">Inventory Management System</h2>
    </header>
    
    <section class="vcss-section">
        <div class="vcss-div">
            <h3>Manage Customers</h3>
            <hr>
            <form action="/add-customer" method="GET">
                @csrf
                <button class="btn vcss-btn btn-prime" type="submit">Add Customer</button>
            </form>
            <div>
                <table class="text-center table table-striped my-2">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Address</th>
                            <th>Email</th>
                            <th>Contact</th>
                            @auth
                            @if (Auth::user()->role_id <= 2)
                            <th>Action</th>
                            @endif
                            @endauth
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($customers as $customer)
                        <tr>
                            <td>{{$customer->id}}</td>
                            <td>{{$customer->name}}</td>
                            <td>{{$customer->address}}</td>
                            <td>{{$customer->email}}</td>
                            <td>{{$customer->contact}}</td>
                            @auth
                            @if (Auth::user()->role_id <= 2)
                            <td class="td-action">
                                <form action="/update-customer/{{$customer->id}}" method="GET">
                                    @csrf
                                    <button class="btn vcss-btn">
                                        <img class="action-icons" src="{{asset('images/icons/edit.svg')}}" alt="">
                                    </button>
                                </form>
                                <form action="/delete-customer/{{$customer->id}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn vcss-btn">
                                        <img class="action-icons" src="{{asset('images/icons/delete.svg')}}" alt="">
                                    </button>
                                </form>
                            </td>
                            @endif
                            @endauth
                        </tr>   
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <footer class="vcss-footer">
        <p class="f-text">Created By: Me</p>
        <p class="f-text">Powered by Laravel 7v</p>
    </footer>
</div>
@endsection