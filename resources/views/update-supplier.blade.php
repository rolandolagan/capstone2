@extends('layouts.template')

@section('title', 'Suppliers')

@section('content')
<div class="vcss-main-div">
    <header class="vcss-header">
        <h2 class="h-header">Inventory Management System</h2>
    </header>
    <section class="vcss-section">
        <div class="vcss-div">
            <h3>Update Supplier</h3>
            <hr>
            <div class="supplier-edit-wrapper">
                <legend>Supplier Information</legend>
                <hr>
                <form action="/update-supplier/{{$supplier->id}}" method="POST">
                    @csrf
                    @method('PATCH')
                    <div class="form-group">
                        <label for="">Fullname</label>
                        <input class="form-control" type="text-area" name="name" value="{{$supplier->name}}" required>
                    </div>
                    <div class="form-group">
                        <label for="">Address</label>
                        <input class="form-control" type="text" name="address" value="{{$supplier->address}}" required>
                    </div>
                    <div class="form-group">
                        <label for="">Email</label>
                        <input class="form-control" type="email" name="email" value="{{$supplier->email}}" required>
                    </div>
                    <div class="form-group">
                        <label for="">Contact No.</label>
                        <input class="form-control" type="number" name="contact" value="{{$supplier->contact}}" required>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-prime vcss-btn btn-block" type="submit">Submit</button>
                        <a class="btn btn-back vcss-btn btn-block" href="/manage-brands" type="button">Back</a>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <footer class="vcss-footer">
        <p class="f-text">Created By: Me</p>
        <p class="f-text">Powered by Laravel 7v</p>
    </footer>
</div>
@endsection