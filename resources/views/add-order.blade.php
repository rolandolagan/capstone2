@extends('layouts.template')

@section('title', 'Orders')

@section('content')
<div class="vcss-main-div">
    <header class="vcss-header">
        <h2 class="h-header">Inventory Management System</h2>
    </header>
    <section class="vcss-section">
        <div class="vcss-div">
            <h3>Order Form</h3>
            <hr>
            <div class="ord-wrapper">
                @if (Session::has("message"))
                    <span class="text-success">{{Session::get('message')}}</span>
                @endif
                <table class="text-center table table-striped my-2">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Category</th>
                            <th>Brand</th>
                            <th>Description</th>
                            <th>Selling Price</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($products as $product)
                        <tr>
                            <td>{{$product->id}}</td>
                            <td>{{$product->name}}</td>
                            <td>{{$product->category->name}}</td>
                            <td>{{$product->brand->name}}</td>
                            <td>{{$product->description}}</td>
                            <td>{{$product->selling_price}}</td>
                            <td class="td-action">
                                <form action="/add-ordcart/{{$product->id}}" method="POST">
                                    @csrf
                                    <button class="btn btn-primary">Add</button>
                                </form>
                                {{-- <button class="btn btn-primary addProdToOrd" data-id="{{$product->id}}">Add</button> --}}
                            </td>
                        </tr>   
                        @endforeach
                    </tbody>
                </table>
                <div class='form-group'>
                    <form action="/order-info" method="GET">
                        <button class="btn btn-primary">Proceed</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <footer class="vcss-footer">
        <p class="f-text">Created By: Me</p>
        <p class="f-text">Powered by Laravel 7v</p>
    </footer>
</div>
{{-- this script is for adding product withouth using form tag --}}
{{-- <script type="text/javascript" defer>
    const addProdToOrds = document.querySelectorAll('.addProdToOrd')
    addProdToOrds.forEach(function(indivAdd){
        indivAdd.addEventListener('click', function(e){
            const prod_id = e.target.getAttribute('data-id')

            let data = new FormData
            data.append("_token", "{{csrf_token()}}")
            // fetch('url who will process the add product')
            fetch('/add-ordcart/' + prod_id, {
                // next an object
                method: "POST",
                body: data
            }).then(function(response){
                return response.text()
            }).then(function(data){
                console.log(data)
            })
        })
    })
</script> --}}

{{-- <script src="{{asset('js/order.js')}}" defer></script> --}}
@endsection