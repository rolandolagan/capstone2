@extends('layouts.template')

@section('title', 'Profile')

@section('content')
<div class="vcss-main-div">
    <header class="vcss-header">
        <h2 class="h-header">Inventory Management System</h2>
    </header>
    <section class="vcss-section">
        <div class="profile-wrapper">
            <legend>User Information</legend>
            <hr>
            <form class="" action="/update-profile/{{Auth::user()->id}}" method="POST">
                @csrf
                @method('PATCH')
                <fieldset>
                    <div class="form-group">
                        <label for="">First Name</label>
                        <input class="form-control" type="text" name="fname" value="{{$user->fname}}" required>
                    </div>
                    <div class="form-group">
                        <label for="">Last Name</label>
                        <input class="form-control" type="text" name="lname" value="{{$user->lname}}" required>
                    </div>
                    <div class="form-group">
                        <label for="">Email</label>
                        <input id="email" class="form-control @error('email') is-invalid @enderror" type="text" name="email" value="{{$user->email}}" required>
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{$message}}</strong>
                            </span>
                        @enderror
                    </div>
                    {{-- <div class="form-group">
                        <label for="">Password</label>
                        <input id="password" class="form-control @error('password') is-invalid @enderror" type="password" name="password">
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{$message}}</strong>
                            </span>
                        @enderror
                    </div> --}}
                    {{-- <div class="form-group">
                        <label for="">Confirm</label>
                        <input class="form-control" type="password" name="password_confirmation">
                    </div> --}}
                    <div class="form-group">
                        <button class="btn btn-prime vcss-btn btn-block" type="submit">Submit</button>
                        <a class="btn btn-back vcss-btn btn-block" href="/manage-profile" type="button">Back</a>
                    </div>
                </fieldset>
            </form>
        </div>
    </section>
<footer class="vcss-footer">
    <p class="f-text">Created By: Me</p>
    <p class="f-text">Powered by Laravel 7v</p>
</footer>
</div>
@endsection