<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});
Auth::routes();

Route::get('/login','Auth\LoginController@index');
Route::post('/login','Auth\LoginController@login');
Route::post('/logout','Auth\LoginController@logout');
// Route::get('/register', 'Auth\RegisterController@index');
// Route::post('/register','Auth\RegisterController@register');

// middleware for status if active or inactive
Route::middleware('status')->group(function(){
    Route::middleware('clerk')->group(function(){
        Route::get('/dashboard','HomeController@index');
        Route::get('/manage-profile','UserController@profile');
        Route::get('/update-profile/{id}','UserController@editProfile');
        Route::patch('/update-profile/{id}','UserController@updateProfile');

        Route::get('/update-password/{id}','UserController@editPass');
        Route::patch('/update-password/{id}','UserController@updatePass');
    });

// middleware for admin/owner
    Route::middleware('check')->group(function(){
    
    Route::get('/manage-users','UserController@index');
    Route::get('/add-user','UserController@create');
    Route::post('/add-user','UserController@store');
    Route::get('/update-user/{id}','UserController@edit');
    Route::patch('/update-user/{id}','UserController@update');
    Route::delete('delete-user','UserController@destroy');

    Route::get('/manage-categories','CategoryController@index');
    Route::get('/add-category','CategoryController@create');
    Route::post('/add-category','CategoryController@store');
    Route::get('/update-category/{id}','CategoryController@edit');
    Route::patch('/update-category/{id}','CategoryController@update');
    Route::delete('/delete-category/{id}','CategoryController@destroy');
    
    Route::get('/manage-brands','BrandController@index');
    Route::get('/add-brand','BrandController@create');
    Route::post('/add-brand','BrandController@store');
    Route::get('/update-brand/{id}','BrandController@edit');
    Route::patch('/update-brand/{id}','BrandController@update');
    Route::delete('/delete-brand/{id}','BrandController@destroy');
    
    Route::get('/manage-suppliers','SupplierController@index');
    Route::get('/update-supplier/{id}','SupplierController@edit');
    Route::patch('/update-supplier/{id}','SupplierController@update');
    Route::delete('/delete-supplier/{id}','SupplierController@destroy');
    Route::get('/add-supplier','SupplierController@create');
    Route::post('/add-supplier','SupplierController@store');

    Route::get('/update-customer/{id}','CustomerController@edit');
    Route::patch('/update-customer/{id}','CustomerController@update');
    Route::delete('/delete-customer/{id}','CustomerController@destroy');
     
    Route::get('/add-product','ProductController@create');
    Route::post('/add-product','ProductController@store');
    Route::get('/update-product/{id}','ProductController@edit');
    Route::patch('/update-product/{id}','ProductController@update');
    Route::delete('/delete-product/{id}','ProductController@destroy');

    Route::get('/update-order/{id}','OrderController@edit');
    Route::patch('/update-order/{id}','OrderController@update');
    Route::delete('/delete-order/{id}','OrderController@destroy');
    });
// end of admin/owner middleware

    Route::get('/manage-customers','CustomerController@index');
    Route::get('/add-customer','CustomerController@create');
    Route::post('/add-customer','CustomerController@store');
    // Route::get('/customer-order/{id}','CustomerController@orderForm');
    
    Route::get('/manage-products','ProductController@index');
    Route::get('/purchase-stock','ProductController@pform');
    Route::patch('/purchase-stock','ProductController@addStock');

    Route::get('/manage-orders','OrderController@index');
    Route::get('/add-order','OrderController@create');
    Route::post('/add-order','OrderController@store');
    Route::get('/order-details/{id}','OrderController@details');
    Route::get('/invoice/{id}','OrderController@invoice');

    Route::post('/add-ordcart/{id}','OrdCartController@addproduct');
    Route::get('/add-ordcart','OrdCartController@flash');
    Route::get('/order-info','OrdCartController@orderinfo');
    Route::post('/update-qty/{id}','OrdCartController@updateQty');
    Route::get('/remove-prod/{id}','OrdCartController@remove');
});
