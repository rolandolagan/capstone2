<?php

use Illuminate\Database\Seeder;

class PaymentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('payments')->delete();
        db::table('payments')->insert(array(
            0 => array(
                'id' => 1,
                'name' => 'Unpaid',
                'created_at'=>now(),
                'updated_at'=>now()
            ),
            1 => array(
                'id' => 2,
                'name' => 'Paid',
                'created_at'=>now(),
                'updated_at'=>now()
            )
        ));
    }
}
