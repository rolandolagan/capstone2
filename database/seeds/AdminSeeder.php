<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        DB::table('users')->insert(array(
            0 => array(
                'id'=>1,
                'fname'=>'Admin',
                'lname'=>'Admin',
                'email'=>'admin@gmail.com',
                'password'=>Hash::make('adminpass'),
                'role_id'=>1,
                'status_id'=>1,
                'created_at'=>now(),
                'updated_at'=>now()
            )
        ));
    }
}
